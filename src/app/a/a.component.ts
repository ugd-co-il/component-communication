import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-a',
  templateUrl: './a.component.html',
  styleUrls: ['./a.component.css']
})
export class AComponent implements OnInit {
  adata = '';
  bdata = '';

  constructor() { }

  bDataUpdated(val) {
    this.bdata = val;
  }

  setAData(val) {
    this.adata = val;
  }

  ngOnInit() {
  }

}
