import { Component, Output, Input, OnInit, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-b',
  templateUrl: './b.component.html',
  styleUrls: ['./b.component.css']
})
export class BComponent implements OnInit {
  @Input() adata = '';
  @Output() bdata = new EventEmitter();

  constructor() { }

  setBData(val) {
    this.bdata.emit(val);
  }

  ngOnInit() {
  }

}
