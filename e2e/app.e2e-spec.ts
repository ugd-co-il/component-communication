import { ComponentComPage } from './app.po';

describe('component-com App', () => {
  let page: ComponentComPage;

  beforeEach(() => {
    page = new ComponentComPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
